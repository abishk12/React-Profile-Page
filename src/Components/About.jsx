import React, { Component } from 'react'
import biologo from './public/image/biopicimg.jpg'

class About extends Component {
    render() {
        return (
            <section id="container-about" className="container-about">
                    <h1>About Me</h1> 

                    <p>Etiam vel ipsum. Morbi facilisis vestibulum  nisl. Praesent cursus laoreet felis. Integer adipiscing pretium  orci. Nulla facilisi. Quisque posuere bibendum purus. Nulla quam  mauris, cursus eget, convallis ac, molestie non, enim. Aliquam  congue. Quisque sagittis nonummy sapien. Proin molestie sem vitae  urna. Maecenas lorem. Vivamus viverra consequat enim.</p>
                
            </section>
        )
    }
}

export default About
