import React, { Component } from 'react'
import pybot from '../Components/public/image/pybot.png';
import Opencv from '../Components/public/image/Snapshot.png';
import NightMares from '../Components/public/image/menu3.png';




class Portfolio extends Component {
    render() {
        return (
            
            <section className="projects">
                  <h1 id='Portfolio'>Portfolio</h1>
                  <p>Some projects</p>

                    <div className="container">
                        <a href="https://github.com/abishk12/Python_projectCFA-Insta"><img src={pybot} width="200" height="200" alt="Pybot"/><p>Pybot:Chat Based on NLP</p></a>
                        <a href="https://github.com/Eloic-F/EloicFerdinand_2_30042021"><img src={Opencv} width="200" height="200" alt="Pybot"/><p>Drowsiness-monitoring-Using-OpenCV-Python</p></a>
                        <a href="https://github.com/Eloic-F/CV_Ferdinand"><img src={NightMares} width="200" height="200" alt="Pybot"/><p>Nigthmares FPS</p></a>
                    </div>
                       
            </section>
        )
    }
}

export default Portfolio


